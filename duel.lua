local Language = SystemData.Settings.Language.active
local DELAY = 30
local Timer

LabelChallanger = L"duel"
challanger = L""
challanger2 = L""
IsChallanger = 0

RegisterEventHandler( SystemData.Events.ENTER_WORLD, "AfterLoad" )
RegisterEventHandler( SystemData.Events.INTERFACE_RELOADED, "AfterLoad" )	

if Language == 1 then --English
Word1 = L"([%a]+) challenges you to a duel."			--Getting challanged by someone
Word2 = L"([%a]+) surrenders to you in a duel."			--You win or enemy gives up
Word3 = L"You surrender your duel."						--You lose or gives up
Word4 = L"The offer to duel has been cancelled."		--You or enemy cancels the duel
Word5 = L"([%a]+) has accepted your duel! ([^%.]+)."	--Enemy accepts an duel
Word6 = L"You have challenged ([%a]+) to a duel."		--You Challange someone
Word7 = L" declines your duel."							--Enemy declines an duel

elseif Language == 2 then --Fracais
Word1 = L"([%a]+) vous a provoqué en duel."	--ok
Word2 = L"([%a]+) a décidé de se rendre."	
Word3 = L"Vous décidez de vous rendre."
Word4 = L"La demande de duel a été annulée."	--ok
Word5 = L"([%a]+) a accepté votre duel ! Le combat commencera dans 5 secondes !"	--ok
Word6 = L"Vous avez provoqué ([%a]+) en duel."	--ok
Word7 = L" a refusé le duel."	--ok


elseif Language == 3 then --German
Word1 = L"([%a]+) fordert Euch zu einem Duell heraus."	--ok
Word2 = L"([%a]+) gibt sich im Duell geschlagen."	--ok
Word3 = L"Ihr gebt Euer Duell auf."	--ok
Word4 = L"Die Forderung nach einem Duell wurde abgelehnt."	--ok
Word5 = L"([%a]+) hat Euer Duell akzeptiert! ([^%.]+)."	--ok
Word6 = L"Ihr habt ([%a]+) zu einem Duell gefordert."	--ok
Word7 = L" hat Euer Duell abgelehnt."	--ok

elseif Language == 10 then --Russian

Word1 = L"([%a]+) вызывает вас на дуэль."			--Getting challanged by someone
Word2 = L"([%a]+) сдается вам."			--You win or enemy gives up
Word3 = L"Вы сдались."						--You lose or gives up
Word4 = L"Вызов на дуэль отменен."		--You or enemy cancels the duel
Word5 = L"([%a]+) принимает ваш вызов! ([^%.]+)."	--Enemy accepts an duel
Word6 = L"Вы вызвали игрока ([%a]+) на дуэль."		--You Challange someone
Word7 = L" отказывается от дуэли."							--Enemy declines an duel

end




function xxxinitilize()

	RegisterEventHandler(TextLogGetUpdateEventId("System"), "duel_PlayerRenownUpdated");
	RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "duel_UpdateProcessed")
 	CreateWindow("duelwindow", false)
 	CreateWindow("CancelWindow", false)
 	CreateWindow("AcceptWindow", false)
	CreateWindow("DeclineWindow", false)
	
	LayoutEditor.RegisterWindow( "duelwindow", L"Duel", L"Duel", false, false, true, nil )	
	
	
	
	ButtonSetText("AcceptButton",L"Accept")
	ButtonSetText( "DeclineButton",	L"Decline")	
	ButtonSetText("CancelButton",L"Cancel")	

	TextLogAddEntry("Chat", 0, L"<icon=57> Duel v0.5 Loaded.")	
end


function duel_UpdateProcessed(elapsed)
if Timer ~= nil then  Timer = Timer - elapsed 

	LabelSetText("LabelTimer", L""..math.ceil(Timer))
--	WindowClearAnchors("CancelWindow")
--	WindowAddAnchor("CancelWindow","center","duelwindow","center",tonumber(Timer)*10,0)
	
		
	if (Timer <= 0) then
 --       UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "duel_UpdateProcessed")
        duel_TimeOut()
        Timer = nil 
 end

end
end


function duel_PlayerRenownUpdated(updateType, filterType)	
	if ( updateType ~= SystemData.TextLogUpdate.ADDED ) then  --check if new entrys to chat
		return 
	end

	if not ( filterType == ChatSettings.Channels[2000].id)then --Listens for System General chats
		return 
	end

    local msg
    local num = TextLogGetNumEntries("System") - 1
    local PlayerName = wstring.sub( GameData.Player.name,1,-3 )
    _, _, msg = TextLogGetEntry("System", num);    

	if 	msg:match(Word1) then --Incomming challange
		challanger = msg:match(Word1)
		
		if (challanger ~= nil) then --If the challanger has a name
			IsChallanger = 0
			TextLogAddEntry("Chat",0 , L"Challanged by: "..towstring(challanger))
			WindowSetShowing("duelwindow", true)
			WindowSetShowing("CancelWindow", false)
			WindowSetShowing("AcceptWindow", true)
			WindowSetShowing("DeclineWindow", true)
			LabelSetText("LabelChallanger",L"")
			LabelSetText("LabelChallanger_row2",challanger)
			LabelSetText("LabelChallanger_row3", L" Challenge You To a Duel!")
			SystemData.AlertText.VecType = {16}
			SystemData.AlertText.VecText = {L""..challanger..L" Challenge You To a Duel!"}
			AlertTextWindow.AddAlert()
			PlaySound(215)
			Timer = (DELAY)-1
--			RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "duel_UpdateProcessed")
			end

	elseif msg:match(Word2) then --you win
		SystemData.AlertText.VecType = {16}
		SystemData.AlertText.VecText = {L""..challanger..L" Surrenders.<br>You Win the Duel!"}
		AlertTextWindow.AddAlert()
		PlaySound(215)
		Timer = nil	
	elseif msg:match(Word3) then --you lose
		SystemData.AlertText.VecType = {16}
		SystemData.AlertText.VecText = {L"You Surrender to "..challanger..L"<br> You Lose The Duel!"}
		AlertTextWindow.AddAlert()
		PlaySound(218)
		Timer = nil
	
	elseif msg:match(Word4) then --The Duel was canceled	
		
		WindowSetShowing("duelwindow", false)
			WindowSetShowing("CancelWindow", false)
			WindowSetShowing("AcceptWindow", false)
			WindowSetShowing("DeclineWindow", false)
		SystemData.AlertText.VecType = {16}
		SystemData.AlertText.VecText = {L"The Duel Was Cancelled!"}
		AlertTextWindow.AddAlert()
		PlaySound(218)
	--	UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "duel_UpdateProcessed")
        Timer = nil
	
	elseif msg:match(Word5) then --The Duel was accepted
		challanger,_ = msg:match(Word5)

		WindowSetShowing("duelwindow", false)
			WindowSetShowing("CancelWindow", false)
			WindowSetShowing("AcceptWindow", false)
			WindowSetShowing("DeclineWindow", false)
		SystemData.AlertText.VecType = {16}
		SystemData.AlertText.VecText = {challanger..L" Accepted the Duel!"}
		AlertTextWindow.AddAlert()
		PlaySound(218)	
--		UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "duel_UpdateProcessed")
        Timer = nil

	
	elseif msg:match(Word6) then --You Issued a Duel
		challanger = msg:match(Word6)	

			WindowSetShowing("duelwindow", true)
			LabelSetText("LabelChallanger", L"You Challenge")	
			LabelSetText("LabelChallanger_row2",challanger)
			LabelSetText("LabelChallanger_row3", L"To a Duel!")
			WindowSetShowing("CancelWindow", true)
			WindowSetShowing("AcceptWindow", false)
			WindowSetShowing("DeclineWindow", false)
			
		SystemData.AlertText.VecType = {16}
		SystemData.AlertText.VecText = {L"You have Challenged "..challanger..L" to a Duel"}
		AlertTextWindow.AddAlert()
		PlaySound(218)
		Timer = DELAY
--		RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "duel_UpdateProcessed")
		IsChallanger = 1

		
	elseif msg:match(challanger..Word7) then --Target Declines
	
		IsChallanger = 0
		SystemData.AlertText.VecType = {16}
		SystemData.AlertText.VecText = {challanger..L" Declined Your Duel Request"}
		AlertTextWindow.AddAlert()
		PlaySound(218)
	--	UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "duel_UpdateProcessed")
        Timer = nil
			WindowSetShowing("duelwindow", false)
			WindowSetShowing("CancelWindow", false)
			WindowSetShowing("AcceptWindow", false)
			WindowSetShowing("DeclineWindow", false)
	end	
		
		

		
		
end

function duel_Accept()		--You Accepting Duel
	if (GameData.Player.rvrPermaFlagged == true)  or (GameData.Player.rvrZoneFlagged == true) then 	--Must be RvR flagged to Duel!

		SystemData.AlertText.VecType = {16}
		SystemData.AlertText.VecText = {L"You Accepted The Duel!"}
		AlertTextWindow.AddAlert()
		PlaySound(215)
		SendChatText(towstring("/duelaccept"), L"")
		WindowSetShowing("duelwindow", false)
			WindowSetShowing("CancelWindow", false)
			WindowSetShowing("AcceptWindow", false)
			WindowSetShowing("DeclineWindow", false)
--		UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "duel_UpdateProcessed")
        Timer = nil

	else
	
		SystemData.AlertText.VecType = {16}
		SystemData.AlertText.VecText = {L"You Must Be Flagged For RVR to Duel"}
		AlertTextWindow.AddAlert()
	end
	
end

function duel_Decline()		--You Declining Duel
	SystemData.AlertText.VecType = {16}
	SystemData.AlertText.VecText = {L"A Duel With "..challanger..L" Has Been Declined"}
	AlertTextWindow.AddAlert()
	PlaySound(214)
	SendChatText(towstring("/dueldecline"), L"")
	WindowSetShowing("duelwindow", false)
			WindowSetShowing("CancelWindow", false)
			WindowSetShowing("AcceptWindow", false)
			WindowSetShowing("DeclineWindow", false)	
--	UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "duel_UpdateProcessed")
    Timer = nil

end


function duel_Cancel()		--Cancel Duel
	SendChatText(towstring("/duelcancel"), L"")
	WindowSetShowing("duelwindow", false)
			WindowSetShowing("CancelWindow", false)
			WindowSetShowing("AcceptWindow", false)
			WindowSetShowing("DeclineWindow", false)		
--	UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "duel_UpdateProcessed")
    Timer = nil	
end

function duel_TimeOut()
if (IsChallanger == 1) then
duel_Cancel()
Timer = nil
IsChallanger = 0
	else
duel_Decline()
Timer = nil
	end
end

function AfterLoad()

	SendChatText(towstring("/dueldecline"), L"")
	SendChatText(towstring("/duelcancel"), L"")
	WindowSetShowing("duelwindow", false)
			WindowSetShowing("CancelWindow", false)
			WindowSetShowing("AcceptWindow", false)
			WindowSetShowing("DeclineWindow", false)	
--	UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "duel_UpdateProcessed")
    Timer = nil



end
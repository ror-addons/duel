<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="Duel" version="0.5" date="4/2/2016">
		<VersionSettings gameVersion="1.4.8" windowsVersion="1.0" savedVariablesVersion="1.0" /> 
		<Author name="Sullemunk" />
        <Description text="Makes a visual notification window upon dueling" />
        <Dependencies />
        <Files>
            <File name="duel.lua" />
			 <File name="duel.xml" />
        </Files>
        <OnInitialize>
             <CallFunction name="xxxinitilize" />
        </OnInitialize>
        <OnUpdate>

    	  </OnUpdate>
        <OnShutdown />
    </UiMod>
</ModuleFile>